# Łacian post-setup steps

- Install Kakoune plugins with `plug-install`.
- Optionally enable `random-background` in your `~/.gnomerc`.
- Log into your Mozilla account in Firefox.
- Copy your `places.sqlite` over to `~/.mozilla/firefox/<gibberish>/`.
- Import your GnuPG key with `import-gpg-key`.
- Configure git with `configure-git` having completed the step above.
- Generate new SSH keys for your GitHub and GitLab accounts with
  `gen-github-key` and `gen-gitlab-key`.
- Log into your email server in Thunderbird.
- Enable GnuPG encryption in Thunderbird by going through "Account Settings" ->
  "End-To-End Encryption" -> "Add Key", where you can select "Use your external
  key…" and put in your key ID, which you can find with `gpg -K`.
- Link `~/.thunderbird/<gibberish>/abook.sqlite` and
  `~/.thunderbird/<gibberish>/calendar-data/local.sqlite` to their destinations
  on an external drive.
- Load the [VSCode profile](https://lacina.io/lacian/desktop/lacian.code-profile)
  and fix any settings that can be set only in application scope.
- Let the "clangd" extension in VSCode download clangd for you.
- Gradually hide all those annoying little buttons sprinkled everywhere in
  VSCode.
- Configure bm with `bm c`.
