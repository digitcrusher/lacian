#!/bin/sh -e

url=https://lacina.io/lacian/

wget_() { # Aliases don't work for some reason
  wget -nv --backups "$@"
  rm -f .wget-hsts
}

. /usr/share/debconf/confmodule
wget_ $url/setup.templates -P /run/
debconf-loadtemplate lacian /run/setup.templates # This doesn't support reading templates from stdin :(
db_progress START 0 $(grep step /run/setup.templates | wc -l) lacian/title
step() {
  echo "-- Step $1"
  db_progress INFO lacian/step/$1
}
finish() {
  db_progress STEP 1
}

# Don't chroot anywhere now because debconf messages won't work
cd /target/

apt_update() {
  in-target apt update "$@" || true # At this point, the non-existent CD repo is still enabled, so we have to catch the error
}
apt_purge() {
  in-target apt purge --autoremove -y "$@"
}
fix_home_permissions() {
  for file in home/*; do
    chown -hR $(in-target --pass-stdout stat -c '%u:%g' "$file") "$file" # chown and chmod don't have -v in this weird installer environment. We also don't have stat. :(
  done
}

step rootskel # rootskel actually thinks we are visually impaired and installs some extra unneeded stuff, if the installer theme is set to "dark"
db_set debian-installer/theme auto
finish

step after-setup
wget_ $url/extra/after-setup.md -P usr/local/share/doc/lacian/
ln -sv /usr/local/share/doc/lacian/after-setup.md etc/skel/after-setup.md
# No need for this in root/
for user in home/*/; do
  cp -Pv etc/skel/after-setup.md "$user/"
done
fix_home_permissions
for script in configure-git gen-github-key gen-gitlab-key import-gpg-key random-background; do
  wget_ $url/extra/$script -P usr/local/bin/
  chmod +x usr/local/bin/$script
done
finish

step adm
for user in $(in-target --pass-stdout groupmems -lg sudo); do
  in-target adduser --verbose "$user" adm # Access to journal
done
finish

step console-setup
sed -i 's/CODESET=.*/CODESET="Lat2"/' etc/default/console-setup
in-target dpkg-reconfigure -f noninteractive console-setup
finish

step bashrc
echo >> etc/skel/.bashrc
wget_ $url/server/.bashrc -O - >> etc/skel/.bashrc
cp -v etc/skel/.bashrc root/
for user in home/*/; do
  cp -v etc/skel/.bashrc "$user/"
done
finish

step kakoune
apt-install kakoune
in-target update-alternatives --set editor /usr/bin/kak
wget_ $url/server/kakrc -O usr/share/kak/kakrc.local
finish

step ssh
wget_ $url/server/ssh_config -O etc/ssh/ssh_config.d/lacian.conf
finish

step profile
wget_ $url/server/.profile -O etc/profile.d/lacian.sh # Must be .sh
wget_ $url/server/sudo.conf -O etc/sudoers.d/lacian # No dot allowed here
finish

step dev-tools
apt-install g++ git make
in-target git config --system commit.gpgsign true
finish

step gnome
apt-install file-roller gnome-core gnome-shell-extension-appindicator papirus-icon-theme
apt_purge gnome-contacts
wget_ $url/desktop/dconf -P etc/dconf/db/lacian.d/
cat >> etc/dconf/profile/user <<EOF
user-db:user
system-db:lacian
EOF
in-target dconf update
wget_ $url/desktop/.gnomerc -P etc/skel/
cp -v etc/skel/.gnomerc root/
for user in home/*/; do
  cp -v etc/skel/.gnomerc "$user/"
done
fix_home_permissions
finish

step dejavu-sans-mono
apt-install fonts-dejavu
wget_ $url/desktop/fonts.conf -O etc/fonts/local.conf
in-target fc-cache -rv
finish

step gtk3-theme # Don't change the version unless Debian updates libadwaita to >=1.3
wget_ https://github.com/lassekongo83/adw-gtk3/releases/download/v4.3/adw-gtk3v4-3.tar.xz -O - | unxz | tar -x
mkdir -pv usr/local/share/themes/
mv -v adw-gtk3 adw-gtk3-dark usr/local/share/themes/
finish

step accent-colors # Keep the version in sync with GNOME's extension API version
wget_ https://github.com/dimitriskp22/custom-accent-colors/releases/download/v6/custom-accent-colors@demiskp.shell-extension.zip -O extension.zip
mkdir -pv usr/local/share/gnome-shell/extensions/
in-target unzip /extension.zip -d /usr/local/share/gnome-shell/extensions/custom-accent-colors@demiskp/ # There's no way for us to get unzip in the installer environment
rm -v extension.zip
chmod -R +r usr/local/share/gnome-shell/extensions/custom-accent-colors@demiskp/ # The permissions in the zip are wrong
wget_ https://git.io/papirus-folders-install -O - | in-target sh
in-target papirus-folders -vC red -t Papirus-Dark
finish

step firefox
wget_ $url/desktop/firefox.js -O etc/firefox-esr/lacian.js
finish

step kakoune-gui
apt-install kitty xclip
wget_ $url/desktop/kakoune.desktop -P usr/local/share/applications/
wget_ $url/desktop/kitty.conf -P etc/xdg/kitty/
wget_ $url/desktop/mimeapps.list -P etc/xdg/
if [ "$(dmidecode -s system-product-name)" = VirtualBox ]; then # Ain't no hardware acceleration in VirtualBox
  sed -i 's/kitty/LIBGL_ALWAYS_SOFTWARE=true kitty/' usr/local/share/applications/kakoune.desktop
  sed -i 's/^Exec=/Exec=env LIBGL_ALWAYS_SOFTWARE=true /' usr/share/applications/kitty.desktop
fi
finish

step yt-dlp
wget_ $url/desktop/yt-dlp.list -P etc/apt/sources.list.d/
apt_update
apt-install yt-dlp/stable-backports
wget_ $url/desktop/yt-dlp.conf -P etc/
finish

step mpv
apt-install mpv mpv-mpris
wget_ $url/desktop/mpv.conf -P etc/mpv/
apt_purge totem
finish

step keepassxc
apt-install keepassxc
wget_ $url/desktop/keepassxc.ini -P etc/skel/.config/keepassxc/
# No need for this in root/
for user in home/*/; do
  cp -rv etc/skel/.config/ "$user/"
done
fix_home_permissions
finish

step thunderbird
apt-install thunderbird
wget_ $url/desktop/thunderbird.js -O etc/thunderbird/pref/lacian.js
finish

step vscode
apt-install extrepo
in-target extrepo enable vscodium
apt_update
apt-install codium # We could install clangd here but the version in stable is too old
finish

step sbt
wget_ $url/desktop/sbt.list -P etc/apt/sources.list.d/
wget_ 'https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823' -O etc/apt/trusted.gpg.d/sbt.asc
apt_update
apt-install default-jre sbt
finish

step bm
in-target git clone https://gitlab.com/digitcrusher/bm.git /opt/bm/
ln -sv /opt/bm/main.py usr/local/bin/bm
apt-install python3-yaml
finish

step libreoffice
apt-install libreoffice libreoffice-gnome
wget_ $url/desktop/libreoffice.xcd -O etc/libreoffice/registry/lacian.xcd
finish

step extra # gnome-disk-utility should not be used for partitioning because the devs forgot to ask the user to confirm their actions
apt-install aspell-pl curl dconf-editor fonts-recommended ghex gparted hunspell-pl hyperfine lzip python3-matplotlib rsync
finish

# apt leaves behind this weird ".cache" directory in /target/ and I don't what to do about it
rm -rfv .cache/
