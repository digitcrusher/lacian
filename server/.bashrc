# The default format (gnu) has some deficiences, e.g. no nanosecond mtime
alias tar='tar --posix'

# I don't know why this isn't on by default
alias dd='dd status=progress'
alias g++='g++ -std=c++20'
alias diff='diff --color=auto'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# The best compression level is a quirky personal preference of mine
alias lzip='lzip --best'

# Handy aliases
alias la='ls -A'
alias ll='ls -ahl'
alias diffw='git diff --no-index --word-diff'
find0() {
  declare -i i=1
  while [[ ${!i} && ${!i} != -* ]]; do
    i+=1
  done
  if [ $i -le $# ]; then
    # See "Operator precedence surprises" in find's manpage
    find "${@:1:i-1}" \( "${@:i}" \) -print0
  else
    find "$@" -print0
  fi
}

# The time I downloaded a file is personally more important for me than the arbitrary time sent back by the server
alias wget='wget --no-use-server-timestamps'

# Enable alias expansion of the command passed to xargs and sudo
alias xargs='xargs '
alias xargs0='xargs -0 '
alias sudo='sudo '

# Make xclip clip data to the clipboard clipboard, and not the other clipboard
alias xclip='xclip -selection clipboard'

# For times when you accidentally enter your password in the wrong place
wipe-history() {
  file=${HISTFILE:-~/.bash_history} # Tilde expansion gives up when the parameter expansion is in quotes
  shred "$file" && history -c && history -w
}

# Color the prompt nicely
red='\[\e[1;31m\]'
green='\[\e[1;32m\]'
blue='\[\e[1;34m\]'
white='\[\e[1;37m\]'
reset='\[\e[0m\]'
PS1="$white"'${debian_chroot:+($debian_chroot)}'"$red\u$white@$green\h$blue \w$reset "
unset red green blue white reset
if [ $EUID -eq 0 ]; then
  PS1+='# '
else
  PS1+='$ '
fi
case $TERM in xterm*|rxvt*)
  PS1='\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h \w\a\]'"$PS1"
esac
