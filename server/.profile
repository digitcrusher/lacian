# High-contrast color theme for whiptail (debconf/apt frontend) from
# debian-installer, with "black" background changed to "default"
export NEWT_COLORS="\
root=white,default \
border=white,default \
window=white,default \
shadow=white,default \
title=yellow,default \
button=black,lightgray \
actbutton=gray,brightred \
checkbox=white,gray \
actcheckbox=black,lightgray \
entry=white,default \
label=brightred,default \
listbox=white,default \
actlistbox=yellow,blue \
textbox=white,default \
acttextbox=lightgray,default \
helpline=white,default \
roottext=yellow,blue \
fullscale=default \
emptyscale=blue \
disentry=blue,lightgray \
compactbutton=white,default \
actsellistbox=black,lightgray \
sellistbox=black,brown"

# Set the default interactive text editor to everyone's favorite Kakoune
export VISUAL=/usr/bin/kak

# Fix client-side decorations in Qt. We, of course, would like to keep GTK
# and Qt apps consistent appearance-wise but Qt apps aren't really made for
# theming, so we can't.
export QT_QPA_PLATFORM=xcb
