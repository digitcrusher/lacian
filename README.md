# Łacian

A set of scripts and config files that turn plain, boring Debian into my rad bespoke GNU/Linux system.

## Installation

1. Download the Debian netinst ISO, i.e. click the big "Download" button on [Debian's website](https://www.debian.org/).
2. Burn it to some storage medium and boot it up.
3. Select the "Help" entry once you land in the "Debian GNU/Linux installer menu".
4. Type in `auto url=https://lacina.io/lacian/preseed priority=high theme=dark` and proceed with the installation as normal.
5. After the installer has completed its work, you will be automatically booted into your fresh new Łacian system.
6. Complete the setup by following the steps in `~/after-setup.md`.

The whole Łacian metamorphosis process is logged to `/var/log/installer/syslog`, so look there, if any errors come up during setup.
