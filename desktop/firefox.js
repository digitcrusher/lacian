// Remove the size cap on browser history because I'm a history preservation freak
pref('places.history.expiration.max_pages', 2147483647);
pref('places.history.expiration.transient_current_max_pages', 2147483647);

// Hide the bookmark toolbar
pref('browser.toolbars.bookmarks.visibility', 'never');

// Unfortunately we can't configure other things such as changing the default
// search engine or hiding the "shortcuts" on the home screen :(
