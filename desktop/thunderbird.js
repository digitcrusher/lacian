// Politely ask Thunderbird to give us the option to not be robbed of our GPG key passphrase
pref('mail.openpgp.allow_external_gnupg', true);
